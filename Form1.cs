﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttRealtimeYasbu
{
    public partial class RTEventsMain : Form
    {

        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
        public static string server = AttRealtimeYasbu.Properties.Settings.Default.Server;
        public static string IPAddress = AttRealtimeYasbu.Properties.Settings.Default.IPAddress;
        public static int Password = AttRealtimeYasbu.Properties.Settings.Default.Password;
        public static int Port = AttRealtimeYasbu.Properties.Settings.Default.Port;
        string apppath = Application.StartupPath.ToString();
        string _httpString = "";
        string dataAttendance = "";
        string messages = "";

        public RTEventsMain()
        {
            InitializeComponent();
        }

        #region Communication
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
        #endregion
        private void btnConnect_Click(object sender, EventArgs e)
        {
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();
            }

            bIsConnected = axCZKEM1.Connect_Net(IPAddress, Port);
            if(bIsConnected == true)
            {
                string _dtFrom = DateTime.Now.ToString("yyyy-MM-dd");
                string _dtTo = DateTime.Now.ToString("yyyy-MM-dd");
                test_lbl.Text = _dtFrom;
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblState.Text = "Current State:Connected";
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                {
                    this.axCZKEM1.OnFinger += new zkemkeeper._IZKEMEvents_OnFingerEventHandler(axCZKEM1_OnFinger);
                    this.axCZKEM1.OnVerify += new zkemkeeper._IZKEMEvents_OnVerifyEventHandler(axCZKEM1_OnVerify);
                    this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);


                   
                }
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        //When you place your finger on sensor of the device,this event will be triggered
        private void axCZKEM1_OnFinger()
        {
            lbRTShow.Items.Add("RTEvent OnFinger Has been Triggered");
        }

        //After you have placed your finger on the sensor(or swipe your card to the device),this event will be triggered.
        //If you passes the verification,the returned value userid will be the user enrollnumber,or else the value will be -1;
        private void axCZKEM1_OnVerify(int iUserID)
        {
            lbRTShow.Items.Add("RTEvent OnVerify Has been Triggered,Verifying...");
            if (iUserID != -1)
            {
                lbRTShow.Items.Add("Verified OK,the UserID is " + iUserID.ToString());
            }
            else
            {
                lbRTShow.Items.Add("Verified Failed... ");
            }
        }


        //If your fingerprint(or your card) passes the verification,this event will be triggered
        private void axCZKEM1_OnAttTransactionEx(string sEnrollNumber, int iIsInValid, int iAttState, int iVerifyMethod, int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond, int iWorkCode)
        {
            lbRTShow.Items.Add("RTEvent OnAttTrasactionEx Has been Triggered,Verified OK");
            lbRTShow.Items.Add("...UserID:" + sEnrollNumber);
            lbRTShow.Items.Add("...isInvalid:" + iIsInValid.ToString());
            lbRTShow.Items.Add("...attState:" + iAttState.ToString());
            lbRTShow.Items.Add("...VerifyMethod:" + iVerifyMethod.ToString());
            lbRTShow.Items.Add("...Workcode:" + iWorkCode.ToString());//the difference between the event OnAttTransaction and OnAttTransactionEx
            lbRTShow.Items.Add("...Time:" + iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString());

            string attendanceDate = "";
            string data = "";
            string path = apppath + @"\log\RealtimeAttendance" + IPAddress + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";

            int enrollNo = 0;
            int status = 0;

            enrollNo = Convert.ToInt32(sEnrollNumber);
            //2010-01-01 23:00:00
            string time = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString();

            //attendanceDate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Parse(iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + "-" + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString()));
            //attendanceDate = 
            status = iWorkCode;
            _httpString = string.Format("{0}~{1}~{2}~{3}", enrollNo, time, status, IPAddress);
            data = string.Format("{0}~{1}~{2}~{3}", enrollNo, time, status, IPAddress);
            //string _dtFrom = DateTime.Now.ToString("yyyy-MM-dd");
            //string _dtTo = DateTime.Now.ToString("yyyy-MM-dd");
            string _dtFrom = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString();
            string _dtTo = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString();
            string param = _dtFrom + "~" + _dtTo;
            dataAttendance = _httpString;

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Sync Data. ";
            CommonLibrary.Instance.writeLogApp(messages);
            bool result = CommonLibrary.Instance.pushData("pushAttandance", dataAttendance);
            bool res = CommonLibrary.Instance.processTransAttendance("syncTransAttendance", param);
        }

        //When you have enrolled your finger,this event will be triggered and return the quality of the fingerprint you have enrolled
        private void axCZKEM1_OnFingerFeature(int iScore)
        {
            if (iScore < 0)
            {
                lbRTShow.Items.Add("The quality of your fingerprint is poor");
            }
            else
            {
                lbRTShow.Items.Add("RTEvent OnFingerFeature Has been Triggered...Score:　" + iScore.ToString());
            }
        }

        //When you are enrolling your finger,this event will be triggered.
        private void axCZKEM1_OnEnrollFingerEx(string sEnrollNumber, int iFingerIndex, int iActionResult, int iTemplateLength)
        {
            if (iActionResult == 0)
            {
                lbRTShow.Items.Add("RTEvent OnEnrollFigerEx Has been Triggered....");
                lbRTShow.Items.Add(".....UserID: " + sEnrollNumber + " Index: " + iFingerIndex.ToString() + " tmpLen: " + iTemplateLength.ToString());
            }
            else
            {
                lbRTShow.Items.Add("RTEvent OnEnrollFigerEx Has been Triggered Error,actionResult=" + iActionResult.ToString());
            }
        }

        //When you have deleted one one fingerprint template,this event will be triggered.
        private void axCZKEM1_OnDeleteTemplate(int iEnrollNumber, int iFingerIndex)
        {
            lbRTShow.Items.Add("RTEvent OnDeleteTemplate Has been Triggered...");
            lbRTShow.Items.Add("...UserID=" + iEnrollNumber.ToString() + " FingerIndex=" + iFingerIndex.ToString());
        }

        //When you have enrolled a new user,this event will be triggered.
        private void axCZKEM1_OnNewUser(int iEnrollNumber)
        {
            lbRTShow.Items.Add("RTEvent OnNewUser Has been Triggered...");
            lbRTShow.Items.Add("...NewUserID=" + iEnrollNumber.ToString());
        }

        //When you swipe a card to the device, this event will be triggered to show you the card number.
        private void axCZKEM1_OnHIDNum(int iCardNumber)
        {
            lbRTShow.Items.Add("RTEvent OnHIDNum Has been Triggered...");
            lbRTShow.Items.Add("...Cardnumber=" + iCardNumber.ToString());
        }

        //When the dismantling machine or duress alarm occurs, trigger this event.
        private void axCZKEM1_OnAlarm(int iAlarmType, int iEnrollNumber, int iVerified)
        {
            lbRTShow.Items.Add("RTEvnet OnAlarm Has been Triggered...");
            lbRTShow.Items.Add("...AlarmType=" + iAlarmType.ToString());
            lbRTShow.Items.Add("...EnrollNumber=" + iEnrollNumber.ToString());
            lbRTShow.Items.Add("...Verified=" + iVerified.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            string attendanceDate = "";
            string data = "";
            string path = apppath + @"\log\RealtimeAttendance" + IPAddress + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";

            int enrollNo = 0;
            int status = 0;

            enrollNo = Convert.ToInt32("1");
            //2010-01-01 23:00:00
            string sEnrollNumber = "1";
            int iIsInValid = 1;
            int iAttState = 1;
            int iVerifyMethod = 1;
            int iYear = 2020;
            int iMonth = 07;
            int iDay = 1;
            int iHour = 06;
            int iMinute = 20;
            int iSecond = 1;
            int iWorkCode = 1;
            string time = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString();

            //attendanceDate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Parse(iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + "-" + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString()));
            //attendanceDate = 
            status = iWorkCode;
            _httpString = string.Format("{0}~{1}~{2}~{3}", 1, time, status, IPAddress);
            data = string.Format("{0}~{1}~{2}~{3}", enrollNo, time, status, IPAddress);
            //string _dtFrom = DateTime.Now.ToString("yyyy-MM-dd");
            //string _dtTo = DateTime.Now.ToString("yyyy-MM-dd");
            //string _dtFrom = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString();
            //string _dtTo = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString();
            //string param = _dtFrom + "~" + _dtTo;
            dataAttendance = _httpString;

            messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Sync Data. ";
            CommonLibrary.Instance.writeLogApp(messages);
            bool result = CommonLibrary.Instance.pushData("pushAttandance", dataAttendance);
            //bool res = CommonLibrary.Instance.processTransAttendance("syncTransAttendance", param);
        }
    }
}
